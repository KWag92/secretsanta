# -*- coding: utf-8 -*-
from smtplib import SMTP
from smtplib import SMTPException
from email.mime.text import MIMEText
import sys
from random import randint
import imaplib

# Names and email address' of participants must be contained within this dictionary, the keys being the Name of the participant, the values being their email address
EMAIL_LOOKUP = {}
GMAIL_ADDRESS = "TheSecretistSanta@gmail.com"
GMAIL_SMTP = "smtp.gmail.com"
GMAIL_SMTP_PORT = 587
TEXT_SUBTYPE = "plain"

def deleteOutbox(pswd):
	box = imaplib.IMAP4_SSL('imap.gmail.com')
	box.login(GMAIL_ADDRESS, pswd)
	box.select('[Gmail]/Sent Mail')
	box.store("1:*", '+FLAGS', r'(\Deleted)')
	box.expunge()
	box.close()
	box.logout()

def sendMsg(pairings, hostEmail, hostEmailPassword):
	smtObj = SMTP(GMAIL_SMTP, GMAIL_SMTP_PORT)
	smtObj.ehlo()
	smtObj.starttls()
	smtObj.ehlo()
	smtObj.login(user=hostEmail, password=hostEmailPassword)
	for pair in pairings:
		print pair["From"]
		text = "Hello " + pair["From"] + ",\nThe secretist of all Santas has finished pairing secret Santas with their gift recipients. The person you will be getting a gift for will be:\n\n" + pair["To"] + "\n\nSome ground rules:\n\t1.) This is secret Santa......so keep it a secret!\n\t2.) Gifts should be 15-20$\n\t3.) Read rule 1 again, loose lips sink ships people!\n\nWhere and when you ask?\nGifts will be exchanged the 26th of December, before Contact, in the hotel.\n\nI wish you all a very merry Christmas,\nThe Secretist Santa"
		msg = MIMEText(text, TEXT_SUBTYPE)
		msg["Subject"] = "Secret Santa"
		msg["From"] = hostEmail
		msg["To"] = ','.join([EMAIL_LOOKUP[pair["From"]]])
		smtObj.sendmail(hostEmail, EMAIL_LOOKUP[pair["From"]], msg.as_string())
	smtObj.quit()

def pairSantas():
	Senders = EMAIL_LOOKUP.keys()
	Receivers = EMAIL_LOOKUP.keys()
	results = []
	while (len(Senders) != 0):
		SenderIndex = randint(0, len(Senders)-1)
		ReceiverIndex = randint(0, len(Receivers)-1)
		if (Senders[SenderIndex] != Receivers[ReceiverIndex]):
			result = {"From" : Senders.pop(SenderIndex), "To" : Receivers.pop(ReceiverIndex)}
			results.append(result)
	return results

def main():
	hostEmailPassword = raw_input('Enter the password for ' + GMAIL_ADDRESS + ': ')
	pairings = pairSantas()
	sendMsg(pairings, GMAIL_ADDRESS, hostEmailPassword)
	deleteOutbox(hostEmailPassword)

if __name__ == "__main__":
	main()